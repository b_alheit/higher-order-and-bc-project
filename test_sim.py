from modules.OneDSim import OneDSim
import scipy.sparse as sp
import numpy as np
from modules import time_stepping_schemes as ts
import matplotlib.pyplot as plt


a = 1
epsilon = 0.1
N = 100

x_mesh = np.linspace(0, 1, N)
t_final = 2
dt = 0.0001

dx = x_mesh[1] - x_mesh[0]

P = np.eye(N, N)
P[0,0] = 0.5
P[-1,-1] = 0.5
P *= dx

# Dx = np.zeros([N,N])
# for i in range(1,N-1):
#     Dx[i, i-1:i+2] = [-0.5, 0, 0.5]
# Dx[0, 0:2] = [-1, 1]
# Dx[-1, -2:] = [-1, 1]
# Dx *= 1/dx
# Dx = sp.dia_matrix(Dx)

Qx = np.zeros([N, N])
for i in range(1,N-1):
    Qx[i, i - 1:i + 2] = [-0.5, 0, 0.5]
Qx[0, 0:2] = [-0.5, 0.5]
Qx[-1, -2:] = [-0.5, 0.5]
Dx = np.matmul(np.linalg.inv(P), Qx)



f = lambda x: np.array([1 - x]).T
u = lambda x, t: 1 - np.e**((x-a)/epsilon)
ux = lambda x, t: - (1/epsilon) * np.e**((x-a)/epsilon)
g2 = lambda t: (a * u(0, t) - epsilon * ux(0, t))
g1 = lambda t: -(epsilon * ux(1, t))

sim = OneDSim(a, epsilon, x_mesh, P, Dx, f, g1, g2, t_final, dt)
sim.solve()
sim.add_to_fig()

