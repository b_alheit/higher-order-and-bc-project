\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Stability}{4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Proving stability of the governing equation}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Numerically solving the equation}{6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Governing equation solver: \texttt {OneDSim}}{6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Code to drive \texttt {OneDSim}: \texttt {OneDSimDriver} }{6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Parameters used in the example problem}{7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Results and discussion}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Solution on 20 node mesh}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Convergence study}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3}Truncation convergence study}{9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Conclusion}{11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A}\texttt {OneDSim} and \texttt {OneDSimDriver}}{12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {B}Solution of 20 nodes: \texttt {part3b}}{16}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {C}Convergence study: \texttt {part3c}}{18}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {D}Truncation error study: \texttt {part3d}}{21}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {E}Time stepping schemes: \texttt {time\_stepping\_schemes.\_\_init\_\_.py}}{24}
