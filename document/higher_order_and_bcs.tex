\documentclass{article}

\author{Benjamin Alheit}
\date{\today}
\title{An investigation into stability, high-order methods, convergence and truncation error when solving the discretised advection-diffusion equation}

\usepackage{graphicx}
\graphicspath{{figures/}}
\usepackage[margin = 0.8in]{geometry}
\usepackage{enumitem}
\usepackage{subcaption}
\usepackage{amsmath} 
\usepackage{amssymb} 
\usepackage{bm}
\usepackage{float}
\usepackage{subfig}
\usepackage[backend=biber]{biblatex}
\usepackage[final]{pdfpages}
\setboolean{@twoside}{false}
\newcommand{\specialcell}[2][c]{\begin{tabular}[#1]{@{}c@{}}#2\end{tabular}}
\bibliography{ref.bib}
%\usepackage[nottoc]{tocbibind}
\usepackage[title]{appendix}

\begin{document}
\maketitle
\pagebreak
\tableofcontents
\pagebreak
\section{Introduction}
The biggest problem facing modern day computational mechanics, and Computational Fluid Dynamics (CFD) in particular, is the sheer magnitude of computational power and time that it takes to complete industrial scale simulations, particularly in the context of turbulence modelling \cite{wang}. Advanced matrix solvers and time integration schemes help to reduce the runtime of simulations, however the CFD community is still not at a level where a sufficiently large industrial simulation can be run over night on a typical High Performance Computing (HPC) cluster \cite{mallan}. In an effort to remedy this, the current trend in CFD is to resort to high-order methods of approximating differentials. High-order methods present different problems, however, in the context of stability and appropriate boundary conditions.\\
\\
In light of this, the following work sets out to prove the stability of the discrete advection-diffusion, governing equation

\begin{equation}
\bm{F}(\bm{U}, t) := \bm{U}_t = - a \mathcal{D}_x \bm{U} + \epsilon \mathcal{D}_{xx} \bm{U} - \mathcal{P}^{-1} \left( \begin{matrix}
aU_0 - \epsilon (\mathcal{D}_x \bm{U})_0 - g_1(t)\\
0\\
\vdots\\
0\\
\epsilon (\mathcal{D}_x \bm{U})_N - g_2(t)
\end{matrix} \right),
\label{eq:adv-diff}
\end{equation}
where $\bm{U}$ is the solution at time $t$, $a$ and $\epsilon$ are strictly positive constants, $\mathcal{D}_x$ is a discrete derivative operator, $\mathcal{D}_{xx}$ is a discrete double derivative operator, $\mathcal{P}$ is a mass-lumped matrix integration operator, $g_1(t)$ and $g_2(t)$ are the left and right boundary conditions respectively and the subscript $t$ denotes the time derivative.\\
\\
This work will also find the steady state solution to this problem using the common low-order finite volume and tridiagonal finite difference methods and a high-order pentadiagonal finite difference method. Additionally, a convergence study will be carried out for each method to determine its convergence order, and the truncation error will investigated to determine if there is a relationship between convergence order and the order of truncation error.

\pagebreak
\section{Stability}
The strong definition of a stable discrete problem is one where
\begin{equation}
\dfrac{\partial}{\partial t} ||\bm{U}||^2_\mathcal{P} \leq \phi(g_i(t)), \quad i = 1,2,..., n_{BCs}.
\end{equation}
Where $n_{BCs}$ is the number of boundary conditions. The weak definition of stability is, 
\begin{equation}
\dfrac{\partial}{\partial t} ||\bm{U}||^2_\mathcal{P} \leq 0
\end{equation}
in the homogeneous case where $g_i(t) = 0$. The weak definition is sufficient in the vast majority of cases.\\
\subsection{Proving stability of the governing equation}
To simplify the reading and writing of the document the following definition is made
\begin{equation}
\bm{G} := \left( \begin{matrix}
aU_0 - \epsilon (\mathcal{D}_x \bm{U})_0 - g_1(t)\\
0\\
\vdots\\
0\\
\epsilon (\mathcal{D}_x \bm{U})_N - g_2(t)
\end{matrix} \right). 
\end{equation}
Additionally, the following relations apply
\begin{equation}
\mathcal{Q}_x = \mathcal{PD}_x,
\label{eq:qx pdx}
\end{equation}
\begin{equation}
\mathcal{D}_{xx} = \mathcal{D}_{x}\mathcal{D}_{x},
\label{eq:dxx dxdx}
\end{equation}
\begin{equation}
\mathcal{Q}_x + \mathcal{Q}_x^T = \left(
  \begin{matrix}
    -1 & & & & \\
    & 0 &&&\\
    & &\ddots && \\
    &&& 0& \\
    & & && 1
  \end{matrix}
\right) =: \mathcal{N}.
\label{eq:qx qxt}
\end{equation}
\\
The stability of the governing equation will be proven by the discrete energy method, wherein the equation is left multiplied by $\bm{U}^T\mathcal{P}$
\begin{equation}
\bm{U}^T\mathcal{P}\bm{U}_t = - a \bm{U}^T\mathcal{P}\mathcal{D}_x \bm{U} + \epsilon \bm{U}^T\mathcal{P}\mathcal{D}_{xx} \bm{U} - \bm{U}^T\mathcal{P}\mathcal{P}^{-1} \bm{G}.
\end{equation}
Using the relations in equation \ref{eq:qx pdx} and \ref{eq:dxx dxdx}, this simplifies to
\begin{equation}
\underbrace{\bm{U}^T\mathcal{P}\bm{U}_t}_{\ref{eq:utp}.1} = \underbrace{- a \bm{U}^T\mathcal{Q}_x\bm{U}}_{\ref{eq:utp}.2} \underbrace{+ \epsilon \bm{U}^T\mathcal{Q}_x\mathcal{D}_{x} \bm{U}}_{\ref{eq:utp}.3} \underbrace{- \bm{U}^T\bm{G}}_{\ref{eq:utp}.4}
\label{eq:utp}
\end{equation}
Term \ref{eq:utp}.1 can be developed as follows to get the time derivative of the square of the $\mathcal{P}$-norm of $\bm{U}$
\begin{equation} 
\begin{gathered}
\bm{U}^T\mathcal{P}\bm{U}_t = \dfrac{1}{2} \dfrac{\partial}{\partial t} (\bm{U}^T\mathcal{P}\bm{U})= \dfrac{1}{2} \dfrac{\partial}{\partial t} ||\bm{U}||^2_\mathcal{P}\\
\boxed{\text{Term \ref{eq:utp}.1: }\bm{U}^T\mathcal{P}\bm{U}_t = \dfrac{1}{2} \dfrac{\partial}{\partial t} ||\bm{U}||^2_\mathcal{P}}.
\end{gathered}
\end{equation}
Since term \ref{eq:utp}.2 is a scalar, the transpose of it is equivalent to itself,
\begin{equation}
- a \bm{U}^T\mathcal{Q}_x\bm{U} = (- a \bm{U}^T\mathcal{Q}_x\bm{U})^T.
\end{equation}
Using this, term \ref{eq:utp}.2 can be developed as follows
\begin{equation}
- a \bm{U}^T\mathcal{Q}_x\bm{U} = - a\dfrac{1}{2} \left(\bm{U}^T(\mathcal{Q}_x + \mathcal{Q}_x ^T)\bm{U} \right)
\end{equation}
then, using the relation in equation \ref{eq:qx qxt}, term \ref{eq:utp}.2 can be rewritten as
\begin{equation}
\boxed{\text{Term \ref{eq:utp}.2: }- a \bm{U}^T\mathcal{Q}_x\bm{U} = - a\dfrac{1}{2} \bm{U}^T\mathcal{N}\bm{U}}.
\end{equation}
Term \ref{eq:utp}.3 can be developed as follows
\begin{equation}
\epsilon \bm{U}^T\mathcal{Q}_x\mathcal{D}_{x} \bm{U} = \epsilon \bm{U}^T\mathcal{Q}_x\mathcal{P}^{-1}\mathcal{P}\mathcal{D}_{x} \bm{U} = \epsilon \bm{U}^T(\mathcal{N} - \mathcal{Q}_x^T)\mathcal{P}^{-1}\mathcal{P}\mathcal{D}_{x} \bm{U}
\end{equation}
Since $\mathcal{P}$ is diagonal, it is also symmetric, $\mathcal{P} ^ {-1} = \mathcal{P} ^ {-T}$,
\begin{equation}
\begin{gathered}
\epsilon \bm{U}^T\mathcal{Q}_x\mathcal{D}_{x} \bm{U} = \epsilon \bm{U}^T\mathcal{N}\mathcal{P}^{-1}\mathcal{P}\mathcal{D}_{x} \bm{U} - \epsilon \bm{U}^T(\mathcal{P}^{-1}\mathcal{Q}_x)^T\mathcal{P}\mathcal{D}_{x} \bm{U} = \epsilon \bm{U}^T\mathcal{N}\mathcal{D}_{x} \bm{U} - \epsilon \bm{U}^T\mathcal{D}_x^T\mathcal{P}\mathcal{D}_{x} \bm{U} \\
= \epsilon [ \bm{U}^T\mathcal{N}\mathcal{D}_{x} \bm{U} - (\mathcal{D}_x\bm{U})^T\mathcal{P}\mathcal{D}_{x} \bm{U}] = \epsilon \bm{U}^T\mathcal{N}\mathcal{D}_{x} \bm{U} - \epsilon ||\mathcal{D}_x\bm{U}||_{\mathcal{P}}^2 \\ 
\therefore \quad \boxed{\text{Term \ref{eq:utp}.3: }\epsilon \bm{U}^T\mathcal{Q}_x\mathcal{D}_{x} \bm{U} = \epsilon \bm{U}^T\mathcal{N}\mathcal{D}_{x} \bm{U} - \epsilon ||\mathcal{D}_x\bm{U}||_{\mathcal{P}}^2 }
\end{gathered}
\end{equation}
Term \ref{eq:utp}.4 does not need any development at this stage. Putting these developed terms back into equation \ref{eq:utp} yields
\begin{equation}
\dfrac{\partial}{\partial t} ||\bm{U}||^2_\mathcal{P}\underbrace{+2\epsilon ||\mathcal{D}_x\bm{U}||_{\mathcal{P}}^2}_{\ref{eq:preMult}.1}  = \underbrace{- a \bm{U}^T\mathcal{N}\bm{U}}_{\ref{eq:preMult}.2} \underbrace{+2\epsilon \bm{U}^T\mathcal{N}\mathcal{D}_{x} \bm{U}}_{\ref{eq:preMult}.3} \underbrace{- 2\bm{U}^T\bm{G}}_{\ref{eq:preMult}.4}
\label{eq:preMult}
\end{equation}
Term \ref{eq:preMult}.1, need not be developed further since it is a positive scalar already. Multiplying term \ref{eq:preMult}.2 out yields
\begin{equation}
- a \bm{U}^T\mathcal{N}\bm{U} = a (U_0^2 - U_N^2).
\end{equation}
Multiplying out term \ref{eq:preMult}.3 yields
\begin{equation}
2\epsilon \bm{U}^T\mathcal{N}\mathcal{D}_{x} \bm{U} = 2\epsilon(U_N(\mathcal{D}_{x} \bm{U})_N - U_0(\mathcal{D}_{x} \bm{U})_0 ).
\end{equation}
Multiplying out term \ref{eq:preMult}.4 yields
\begin{equation}
- 2\bm{U}^T\bm{G} = - 2\bm{U}^T \left( \begin{matrix}
aU_0 - \epsilon (\mathcal{D}_x \bm{U})_0 - g_1(t)\\
0\\
\vdots\\
0\\
\epsilon (\mathcal{D}_x \bm{U})_N - g_2(t)
\end{matrix} \right) = -2\left[U_0(aU_0 - \epsilon (\mathcal{D}_x \bm{U})_0 - g_1(t)) + U_N (\epsilon (\mathcal{D}_x \bm{U})_N - g_2(t)\right].
\end{equation}
Substituting these expanded terms in yields
\begin{equation}
\dfrac{\partial}{\partial t} ||\bm{U}||^2_\mathcal{P} + 2\epsilon ||\mathcal{D}_x\bm{U}||_{\mathcal{P}}^2 = -a (U_0^2 + U_N^2) + 2g_1(t) + 2g_2(t).
\end{equation}
Assuming that $a$ and $\epsilon$ are both positive, the weak definition of stability, i.e. in the homogeneous case where $g_1(t)=g_2(t)=0$, can then verified as follows,
\begin{equation}
\begin{gathered}
\dfrac{\partial}{\partial t} ||\bm{U}||^2_\mathcal{P} \leq \dfrac{\partial}{\partial t} ||\bm{U}||^2_\mathcal{P}  + 2\epsilon ||\mathcal{D}_x\bm{U}||_{\mathcal{P}}^2 = -a (U_0^2 + U_N^2) \leq 0 \\
\Rightarrow \dfrac{\partial}{\partial t} ||\bm{U}||^2_\mathcal{P} \leq 0 
\end{gathered}
\end{equation}
To prove that the governing equation is stable according to the strong definition of stability, some mathematical manipulation is first carried out
\begin{equation}
\begin{gathered}
\dfrac{\partial}{\partial t} ||\bm{U}||^2_\mathcal{P} + 2\epsilon ||\mathcal{D}_x\bm{U}||_{\mathcal{P}}^2 = \dfrac{g_1(t)^2}{a} + \dfrac{g_2(t)^2}{a}  - (a U_0^2 - 2g_1(t)U_0  + \dfrac{g_1(t)^2}{a})  -(a U_N^2 - 2g_2(t)U_N  + \dfrac{g_2(t)^2}{a})\\
\dfrac{\partial}{\partial t} ||\bm{U}||^2_\mathcal{P} + 2\epsilon ||\mathcal{D}_x\bm{U}||_{\mathcal{P}}^2 = \dfrac{1}{a}( g_1(t)^2 + g_2(t)^2  - ((a U_0)^2 - 2ag_1(t)U_0  + g_1(t)^2)  -((a U_N)^2 - 2ag_2(t)U_N  + g_2(t)^2))\\
\dfrac{\partial}{\partial t} ||\bm{U}||^2_\mathcal{P} + 2\epsilon ||\mathcal{D}_x\bm{U}||_{\mathcal{P}}^2 = \dfrac{1}{a}( g_1(t)^2 + g_2(t)^2  - (a U_0 - g_1(t))^2   - (a U_N - g_2(t))^2 ).
\label{eq:stable}
\end{gathered}
\end{equation}
Hence the governing equation can be shown to obey the strong definition of stability as follows,
\begin{equation}
\begin{gathered}
\dfrac{\partial}{\partial t} ||\bm{U}||^2_\mathcal{P} \leq \dfrac{\partial}{\partial t} ||\bm{U}||^2_\mathcal{P}  + 2\epsilon ||\mathcal{D}_x\bm{U}||_{\mathcal{P}}^2 = \dfrac{1}{a}( g_1(t)^2 + g_2(t)^2  - (a U_0 - g_1(t))^2   - (a U_N - g_2(t))^2 ) = \phi(g_1(t), g_2(t))\\
\Rightarrow \dfrac{\partial}{\partial t} ||\bm{U}||^2_\mathcal{P} \leq \phi(g_1(t), g_2(t))
\end{gathered}
\end{equation}

\pagebreak
\section{Numerically solving the equation}
\subsection{Governing equation solver: \texttt{OneDSim}}
A numerical code was written in Python 3 to solve the governing equation, equation \ref{eq:adv-diff}. An Object Orientated Programming (OOP) approach was taken. A class was created for a one-dimensional simulation, \texttt{OneDSim}\footnote{Note that $- a \mathcal{D}_x \bm{U} + \epsilon \mathcal{D}_{xx} \bm{U} = (- a \mathcal{D}_x+ \epsilon \mathcal{D}_{x}\mathcal{D}_{x}) \bm{U}, \; \overline{\mathcal{D}_x}:=- a \mathcal{D}_x+ \epsilon \mathcal{D}_{x}\mathcal{D}_{x}$. In the code $\overline{\mathcal{D}_x}$ is precalculated in the constructor and saved as \texttt{Derivative\_operator}. $U_t$ is then calculated using $\overline{\mathcal{D}_x}$ to improve computational efficiency.}, which can be found in appendix \ref{ap:solver}. The class requires the following inputs for its constructor:
\begin{itemize}
\item Problem parameters $a$ and $\epsilon$ (\texttt{a}, \texttt{epsilon})
\item A \texttt{Numpy} array of grid points (nodes) $\bm{X}$ (\texttt{x})
\item Matrix operators $\mathcal{P}$ and $\mathcal{D}_x$ (\texttt{P}, \texttt{Dx})
\item Continuous functions for $f(x)$, $g_1(t)$ and $g_2(t)$ (\texttt{f}, \texttt{g1}, \texttt{g2})
\item Time step size (\texttt{dt})
\end{itemize}
Additionally, the constructor has the default arguments \texttt{Ut\_norm\_final} and \texttt{debug}. \texttt{Ut\_norm\_final} is the Euclidean norm of $\bm{U}_t$ at which the solving algorithm will stop. Setting the \texttt{debug} parameter to \texttt{True} allows the user to check if the correct boundary conditions have been implemented by check if equation \ref{eq:stable} has been satisfied exactly. Since these are default arguments, if the user chooses to ignore them a value of $1\times10^{-8}$ will be used for \texttt{Ut\_norm\_final} and \texttt{debug} will be set to \texttt{False}. Once the simulation has finished solving, the user can extract the solution by writing \texttt{oneDSimName.U}, where \texttt{oneDSimName} is the name of the reference they assigned to the \texttt{OneDSim} object that they created.

\subsection{Code to drive \texttt{OneDSim}: \texttt{OneDSimDriver} \label{sec:driver}}
In order to easily construct and use a \texttt{OneDSim} object a \texttt{OneDSimDriver} class was written. The \texttt{OneDSimDriver} class can also be seen in appendix \ref{ap:solver}. The class automatically constructs the $\mathcal{P}$ and $\mathcal{D}_x$ matrix operators for a number of discrete differentiation methods. Additionally, it contains functions for the initial conditions, left boundary condition, right boundary condition and analytical solution and values for $a$ and $\epsilon$ for the problem that will be elucidated and investigated in the following section. The class requires the following inputs for its constructor:
\begin{itemize}
\item The number of nodes (\texttt{N})
\item The type of discrete differentiation to be used (\texttt{case})
\end{itemize}
The allowed discrete differentiation methods are \texttt{"tridiagonal finite difference"}, \texttt{"finite volume"} and \texttt{"pentadiagonal finite difference"}. For the \texttt{"tridiagonal finite difference"} and \texttt{"pentadiagonal finite difference"} cases, the \texttt{OneDSimDriver} generates an equispaced mesh. If an equispaced mesh were used for the \texttt{"finite volume"} case, however, it would produce exactly the same $\mathcal{P}$ and $\mathcal{D}_x$ operators as the  \texttt{"tridiagonal finite difference"} case. Hence, to force a difference between the two methods, the \texttt{"finite volume"} case used an equispaced mesh where each node has been perturbed by a value randomly sampled from a normal distribution with a standard deviation of $\dfrac{\Delta x}{4}$, where $\Delta x$ is the equispaced mesh spacing. This would produce a non-equispaced mesh where it is unlikely that any two nodes are far closer to one another than any other two nodes, which would necessitate very small time steps and large simulation times.\\
\\
Additionally to the required inputs, the constructor has default arguments for:
\begin{itemize}
\item Problem parameters $a$ and $\epsilon$ (\texttt{a}, \texttt{epsilon})
\item Time step size (\texttt{dt})
\item The Euclidean norm of $\bm{U}_t$ at which the solving algorithm will stop (\texttt{Ut\_norm\_final})
\item The time integration scheme used (\texttt{ts\_used})
\item A one dimensional computational mesh (\texttt{x\_mesh})
\item The debug parameter (\texttt{debug})
\end{itemize}
The default parameters will work for most cases investigated in the following section, however the user may change them if they wish. The default time integration scheme chosen is Runge-Kutta 4 (\texttt{time\_stepping\_schemes.RK4}), which is taken from a module containing various time integration schemes called \texttt{time\_stepping\_schemes}. This module is displayed in appendix \ref{ap:time}, but is not explained in this text. The \texttt{x\_mesh} argument allows the user to force a certain mesh to be used in the simulation. This is useful in convergence studies if the user wants to implement a certain method of mesh refinement. If the argument is left as \texttt{None} the class will construct its own mesh as outlined above. 

\subsection{Parameters used in the example problem}
The parameters $a$ and $\epsilon$ were chosen to have values of $1$ and $0.1$ respectively. The initial conditions of the probelem were chosen to be $f(x) = 1-x$, although this has no effect on the final solution it affects the time it takes for the simulation to reach steady state. The boundary condition at the left face is $g_1(t) = au(0,t) - \epsilon u_x(0,t)$ and the boundary condition at the right face is $g_2(t) = \epsilon u_x(1,t)$. An analytical steady state solution to the equation is given by $u = 1 - e^{(x-a)/\epsilon}$. Hence, the appropriate boundary conditions to approximate that solution is $g_1(t) = a + e^{-a/\epsilon}(1-a) $ and $g_2(t) = -e^{(1-a)/\epsilon}$, which for the chosen values for $a$ and $\epsilon$ gives $g_1(t) = 1$ and $g_2(t) = -1$

\pagebreak
\section{Results and discussion}

\subsection{Solution on 20 node mesh}
The steady state solution to the governing equation was found on a 20 node mesh as a first comparison of the different methods. The results of this can be seen in figure \ref{fig:results}.
\begin{figure}[!hbt]
\includegraphics[width=\linewidth]{part3b}
\caption{Results of different methods on a 20 node mesh}
\label{fig:results}
\end{figure}
From inspection, it is apparent that the high-order pentadiagonal finite difference method produces a more accurate solution than the low-order methods. Additionally, the tridiagonal finite difference method seems to produce a more accurate solution than the finite volume method. However, it is likely that this is due to the poorer mesh quality of the mesh used for the finite volume method since both methods nominally have the same order of accuracy, as will be verified in the following section. 

\subsection{Convergence study}
The error of an approximation, $\bm{e_h}$, can be determined by
\begin{equation}
\bm{e_h} = \bm{U} - \bm{u}(\overline{\bm{x}}),
\end{equation}
where $\bm{u}(\overline{\bm{x}})$ is an array of values of the analytical solution at the grid points $\overline{\bm{x}}$. The $\mathcal{P}$-norm of the error can then be found by
\begin{equation}
||\bm{e_h}||_\mathcal{P} = \sqrt{\bm{e_h}^T\mathcal{P}\bm{e_h}}. 
\end{equation}
Furthermore, the $\mathcal{P}$-norm of the error is related to the mesh spacing as follows
\begin{equation}
||\bm{e_h}||_\mathcal{P} = \alpha\left(\dfrac{1}{N}\right)^p 
\end{equation}
where $N$ is the number of nodes used in the simulation, $p$ is the convergence order of the method of differentiation and $\alpha$ is some constant. Taking the $\log$ of both sides of the equation gives
\begin{equation}
\log(||\bm{e_h}||_\mathcal{P}) = p \log\left(\dfrac{1}{N}\right) + \log(\alpha) 
\end{equation}
Hence, the order of convergence of the method of differentiation can be determined by running a simulation for various numbers of nodes, calculating $||\bm{e_h}||_\mathcal{P}$, plotting $||\bm{e_h}||_\mathcal{P}$ against $\dfrac{1}{N}$ on a $\log$-$\log$ graph and measuring off the gradient.\\
\\
This procedure was followed for the three different methods investigated in this work. The results\footnote{The results for the finite volume method contain some error due to poor mesh quality. It would be undesirable for this error to fluctuate randomly for different mesh sizes during the convergence study. Hence, to avoid this, the coarsest mesh for the finite volume method was generated as outlined in section \ref{sec:driver} and the subsequent meshes were generated by placing nodes at the mid points of all adjacent nodes. This would cause the error due to poor mesh quality to scale proportionally with mesh refinement instead of fluctuating randomly.} can be see in figure \ref{fig:convergence} and table \ref{tab:convergence}.
\begin{figure}[!hb]
\includegraphics[width=\linewidth]{part3c_good}
\caption{Convergence study of different numerical differentiation methods}
\label{fig:convergence}
\end{figure}
\begin{table}[!hb]
\centering
\caption{Convergence study gradients}
\begin{tabular}{c | c c c}
Method & Grad 1 & Grad 2 & Grad 3\\
\hline \hline
Tridiagonal finite difference & 2.01 & 2.01 & 2.01\\
Finite volume & 3.43 & 2.01 & 2.00 \\
Pentadiagonal finite difference & 2.90 & 2.95 & 2.98
\end{tabular}
\label{tab:convergence}
\end{table}
Figure \ref{fig:convergence} shows that the convergence is indeed linear on a $\log$-$\log$ graph, which conveys confidence in the convergence study. Additionally, the figure shows that the pentadiagonal finite difference method produces less error than the tridiagonal finite difference method which produces less error than the finite volume method for the same number of nodes. However, the error produced by the finite volume method may change depending on how the nodes in the mesh are distributed. The results in table \ref{tab:convergence} show that both the tridiagonal finite difference method and the finite volume method have approximately 2$^\text{nd}$ order convergence rates, and the pentadiagonal finite difference method has approximately a 3$^\text{rd}$ order convergence rate.

\subsection{Truncation convergence study}
The truncation error $\bm{T}_h$ of $\bm{F}$ can be determined using the equation
\begin{equation}
\bm{T}_h = \bm{F}(\bm{u}(\overline{\bm{x}}), t).
\end{equation}
The $\mathcal{P}$-norm of the truncation error can be found similarly to as before,
\begin{equation}
||\bm{T_h}||_\mathcal{P} = \sqrt{\bm{T_h}^T\mathcal{P}\bm{T_h}}. 
\end{equation}
Again, similarly to as before, the $\mathcal{P}$-norm of the truncation error is related to the mesh spacing by 
\begin{equation}
||\bm{T_h}||_\mathcal{P} = \tilde{\alpha}\left(\dfrac{1}{N}\right)^{\tilde{p}},
\end{equation}
where $\tilde{\alpha}$ is some constant and $\tilde{p}$ is the order of the truncation error. Hence, the order of the truncation error can be determined by the equation
\begin{equation}
\log(||\bm{T_h}||_\mathcal{P}) = \tilde{p} \log\left(\dfrac{1}{N}\right) + \log(\tilde{\alpha}).
\end{equation}
A study of the truncation error for each method was carried out in this fashion to determine if there is some relationship between $p$ and $\tilde{p}$. The results of this study are shown in figure \ref{fig:trunc} and table \ref{tab:trunc}.
\begin{figure}[!htb]
\includegraphics[width=\linewidth]{part3d}
\caption{Order of truncation error study}
\label{fig:trunc}
\end{figure}
\begin{table}[!htb]
\centering
\caption{Truncation error gradients}
\begin{tabular}{c | c c c c}
Method & Grad 1 & Grad 2 & Grad 3 & Grad 4\\
\hline \hline
Tridiagonal finite difference & -0.56 & -0.53 & -0.52 & -0.50 \\
Finite volume & -0.61 & -0.53 & -0.52 & -0.51\\
Pentadiagonal finite difference & -0.53 & -0.51 & -0.50 & -0.50
\end{tabular}
\label{tab:trunc}
\end{table}
The figure and table both show that the truncation error order is approximately the same for all the methods investigated in this work. Hence, it is unlikely that there is any relationship between $p$ and $\tilde{p}$. This is, perhaps, an unsurprising result since the truncation error is related to the time derivative and the different methods used in this work all concern spatial derivatives and integration operators.
\pagebreak
\section{Conclusion}
Stability was proven in the weak and strong sense for the governing equation, equation \ref{eq:adv-diff}, by using the discrete energy method.\\
\\
Additionally the governing equation was solved with a numerical code, for various spacial differentiation and integration methods. Namely, the tridiagonal finite difference, finite volume and pentadiagonal finite difference methods. It was found that the pentadiagonal finite difference method had the smallest $\mathcal{P}$-norm error, and the finite volume method had the largest $\mathcal{P}$-norm error. However, the error of the finite volume method may change depending on how the nodes in the mesh are distributed, since the finite volume mesh is generated with random spacing. It was also found that the pentadiagonal finite difference method is a high-order method, with a convergence rate of approximately 3, and the tridiagonal finite difference and the finite volume methods are low-order methods, with convergence rates of approximately 2.\\
\\
Finally, the order of the truncation error was investigated for the different methods. It was found that the order of the truncation error is not related to order of the convergence of the error. Furthermore, it was postulated that this is because the order of the truncation error is related to the time derivative of the scheme. Hence, the spacial differentiation and integration methods do not affect it. Instead it is more likely to be dependent on the time integration scheme used.

\printbibliography
\pagebreak
\begin{appendices}

\section{\texttt{OneDSim} and \texttt{OneDSimDriver}\label{ap:solver}}
\includepdf[pages=-,pagecommand={},width=\textwidth]{_sim_init__.pdf}

\section{Solution of 20 nodes: \texttt{part3b}}
\includepdf[pages=-,pagecommand={},width=\textwidth]{part3b.pdf}


\section{Convergence study: \texttt{part3c}}
\includepdf[pages=-,pagecommand={},width=\textwidth]{part3c.pdf}

\section{Truncation error study: \texttt{part3d}}
\includepdf[pages=-,pagecommand={},width=\textwidth]{part3d.pdf}


\section{Time stepping schemes: \texttt{time\_stepping\_schemes.\_\_init\_\_.py}\label{ap:time}}
\includepdf[pages=-,pagecommand={},width=\textwidth]{ts__init__.pdf}


\end{appendices}

\end{document}
\end{document}
