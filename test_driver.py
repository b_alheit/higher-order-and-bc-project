from modules.OneDSim import OneDSimDriver
from modules import time_stepping_schemes as ts


N = 30
efinal = 1e-12

case = "tridiagonal finite difference"
sim_driver = OneDSimDriver(N, case, dt=9/(N**2), ts_used=ts.FE, Ut_norm_final=efinal, debug=True)

# case = "finite volume"
# sim_driver = OneDSimDriver(N, case, t_final=10, dt=4/(N**2), ts_used=ts.FE, Ut_norm_final=efinal)

# case = "pentadiagonal finite difference"
# sim_driver = OneDSimDriver(N, case, t_final=100, dt=5/(N**2), ts_used=ts.FE, Ut_norm_final=efinal)

# sim_driver = OneDSimDriver(N, case, 0.00001)
# sim_driver = OneDSimDriver(N, case, 0.000001)

# u_an = np.array([sim_driver.u(sim_driver.sim.x, 0)]).T
# sim_driver.sim.U = u_an
# sim_driver.sim.plot_solution()


sim_driver.solve()
sim_driver.sim.plot_solution()

