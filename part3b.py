from modules.OneDSim import OneDSimDriver
from modules import time_stepping_schemes as ts
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
# matplotlib.rc('text', usetex=True)
# matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amsmath}"]

N = 20
case1 = "tridiagonal finite difference"
case2 = "finite volume"
case3 = "pentadiagonal finite difference"

sim_driver1 = OneDSimDriver(N, case1, dt=7/N**2, ts_used=ts.RK4, Ut_norm_final=1e-8)
sim_driver1.solve()
sim_driver2 = OneDSimDriver(N, case2, dt=5/N**2, ts_used=ts.RK4, Ut_norm_final=1e-6)
sim_driver2.solve()
sim_driver3 = OneDSimDriver(N, case3, dt=2/N**2, ts_used=ts.RK4, Ut_norm_final=1e-8)
sim_driver3.solve()

x_an = np.linspace(0, 1, 1000)
u_an = 1 - np.e ** ((x_an - 1)/0.1)

plt.plot(x_an, u_an, color="grey", linewidth=2, label="Analytical solution")

sim_driver1.sim.add_to_fig(label=case1)
sim_driver2.sim.add_to_fig(marker="s", label=case2)
sim_driver3.sim.add_to_fig(marker="o", label=case3)

plt.grid()
plt.legend()
plt.xlabel("$x$", fontsize=14)
plt.ylabel("$u$", fontsize=14)
plt.title("Comparison of solutions produced for different numerical methods on a 20 node mesh",
          fontsize=14,
          # fontweight='bold'
          )
plt.figure(1).set_figwidth(10)
plt.figure(1).set_figheight(5)
plt.figure(1).tight_layout()

# plt.figure(1).savefig("./document/figures/part3b.png")
plt.show()
