import numpy as np
import scipy.optimize as opt


def RK4(state, g, dt):
    Un = state.copy()
    q1 = g(Un)
    q2 = g(Un + 0.5*dt*q1)
    q3 = g(Un + 0.5*dt*q2)
    q4 = g(Un + dt*q3)
    return state + (dt / 6)*(q1 + 2 * (q2 + q3) + q4)

def FE(state, g, dt):
    q = g(state)
    return state + dt * q

def SOBD(state, g, dt, state_n1):
    dtau = (2/3) * dt
    U_tau = state + dtau * (g(state) - (3 * state - 4 *state + state_n1)/(2*dt))
    U_tau_1 = U_tau + dtau * (g(U_tau) - (3 * U_tau - 4 *state + state_n1)/(2*dt))

    o = np.linalg.norm(U_tau_1 - U_tau)
    o_new = np.linalg.norm(U_tau_1 - U_tau)
    while o_new / o > 1e-6:
        U_tau = U_tau_1
        U_tau_1 = U_tau + dtau * (g(U_tau) - (3 * U_tau - 4 * state + state_n1) / (2 * dt))
        o_new = np.linalg.norm(U_tau_1 - U_tau)
    return U_tau_1

def SOBD2(state, g, dt, state_n1, dtau):
    U_tau = state + dtau * (g(state) - (3 * state - 4 *state + state_n1)/(2*dt))
    U_tau_1 = U_tau + dtau * (g(U_tau) - (3 * U_tau - 4 *state + state_n1)/(2*dt))

    o = np.linalg.norm(U_tau_1 - U_tau)
    o_new = np.linalg.norm(U_tau_1 - U_tau)
    while o_new / o > 1e-6:
        U_tau = U_tau_1
        U_tau_1 = U_tau + dtau * (g(U_tau) - (3 * U_tau - 4 * state + state_n1) / (2 * dt))
        o_new = np.linalg.norm(U_tau_1 - U_tau)
    return U_tau_1


def mySOBD(state, g, dt, state_n1):
    U_guess = state + dt * g(state)
    NR_func = lambda U: g(U) - (3*U - 4*state + state_n1)/(2*dt)
    U_n1 = opt.minimize(NR_func, U_guess)
    return U_n1


def Heun(state, g, dt):
    Un = state.copy()
    q1 = g(Un)
    q2 = g(Un + dt*q1)
    return state + (dt / 2)*(q1 + q2)
