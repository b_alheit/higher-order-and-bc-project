import numpy as np
from modules import time_stepping_schemes as ts
import matplotlib.pyplot as plt
import warnings


class OneDSim:
    def __init__(self, a, epsilon, x, P, Dx, f, g1, g2, dt, Ut_norm_final=1e-8,  debug=False):
        self.a = a
        self.epsilon = epsilon
        self.x = x
        self.P = P
        self.Pinv = np.linalg.inv(P)
        self.Dx = Dx
        self.f = f
        self.g1 = g1
        self.g2 = g2
        self.t = 0
        self.dt = dt
        self.debug = debug
        self.Ut_norm_final = Ut_norm_final
        self.U = self.f(self.x)
        self.boundary_array = np.zeros([np.alen(self.x), 1])
        self.Derivative_operator = -self.a * self.Dx + self.epsilon * np.matmul(self.Dx, self.Dx)
        self.Ut = None
        self.calc_Ut()
        self.Norm_avg = None
        self.Norm_avg_prev = 0

    def calc_Ut(self, U=None):
        if U is None:
            U = self.U
        self.calc_boundary_array()
        self.Ut = np.matmul(self.Derivative_operator, U) - np.matmul(self.Pinv, self.boundary_array)
        return self.Ut

    def calc_boundary_array(self, U=None):
        if U is None:
            U = self.U

        self.boundary_array[0, 0] = self.a * U[0, 0] - self.epsilon * np.matmul(self.Dx[0, :], U) - self.g1(self.t)
        self.boundary_array[-1, 0] = self.epsilon * np.matmul(self.Dx[-1, :], U) - self.g2(self.t)

    def check_stability(self):
        self.calc_Ut()
        u_norm = 2 * np.matmul(self.U.T, np.matmul(self.P, self.Ut))[0, 0]
        dxu = np.matmul(self.Dx, self.U)
        dxu_norm = 2 * self.epsilon * np.matmul(dxu.T, np.matmul(self.P, dxu))[0,0]

        LHS = u_norm + dxu_norm

        g1 = self.g1(self.t)
        g2 = self.g2(self.t)
        RHS = (g1**2 + g2**2 - (self.a * self.U[0, 0] - g1) ** 2 - (self.a * self.U[-1, 0] - g2) ** 2) / self.a

        if np.abs(LHS - RHS) > 1e-12:
            warnings.warn("Well-posedness equation not satisfied \nLHS - RHS = " + str(LHS - RHS))

    def solve(self, solver=ts.FE):
        i = 0
        self.Norm_avg = self.Ut_norm_final+1
        while self.Ut_norm_final < self.Norm_avg:

            self.U = solver(self.U, self.calc_Ut, self.dt)
            self.t += self.dt

            if self.debug:
                self.check_stability()

            self.Norm_avg = np.linalg.norm(self.Ut) / np.alen(self.Ut)
            if i%1000 == 0:
                print(i, " Ut norm/len(Ut) = ", self.Norm_avg)
            i += 1
        print(i, " Ut norm/len(Ut) = ", self.Norm_avg)

    def add_to_fig(self, fig_num=1, label="", marker="^", fill="None"):
        plt.figure(fig_num)
        plt.plot(self.x, self.U, marker=marker, markeredgecolor="black", markerfacecolor=fill, color="black", label=label)

    def plot_solution(self, fig_num=1, title="", xlabel="", ylabel="", marker="^", fill="None", grid=True,
                      save_path=None, fig_width=10, fig_hegiht=10, tight_layout=True):
        plt.figure(fig_num)
        plt.plot(self.x, self.U, marker=marker, markeredgecolor="black", markerfacecolor=fill, color="black")
        plt.title(title)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.grid(grid)
        plt.figure(fig_num).set_figwidth(fig_width)
        plt.figure(fig_num).set_figheight(fig_hegiht)
        if tight_layout:
            plt.figure(fig_num).tight_layout()

        if save_path is not None:
            plt.figure(fig_num).savefig(save_path)

        plt.show(1)


class OneDSimDriver:
    def __init__(self, N, case, a=1, epsilon=0.1, dt=0.01, Ut_norm_final=1e-6, ts_used=ts.RK4, x_mesh=None,
                 debug=False):
        self.a = a
        self.epsilon = epsilon
        self.N = N

        if x_mesh is None:
            x_mesh = np.linspace(0, 1, N)

        if case == "tridiagonal finite difference":
            dx = x_mesh[1] - x_mesh[0]

            P = np.eye(N, N)
            P[0, 0] = 0.5
            P[-1, -1] = 0.5
            P *= dx

            Dx = np.zeros([N, N])
            for i in range(1, N-1):
                Dx[i, i-1:i+2] = [-0.5, 0, 0.5]
            Dx[0, 0:2] = [-1, 1]
            Dx[-1, -2:] = [-1, 1]
            Dx *= 1/dx

        elif case == "pentadiagonal finite difference":
            dx = x_mesh[1] - x_mesh[0]

            P = np.eye(N, N)
            P[0, 0] = 17/48
            P[1, 1] = 59/48
            P[2, 2] = 43/48
            P[3, 3] = 49/48
            for i in range(4):
                P[-1-i, -1-i] = P[i, i]
            P *= dx

            Qx = np.zeros([N, N])
            Qx[0, 0:4] = [-1/2, 59/96, -1/12, -1/32]
            Qx[1, 0:5] = [-59/96, 0, 59/96, 0, 0]
            Qx[2, 0:6] = [1/12, -59/96, 0, 59/96, -1/12, 0]
            Qx[3, 0:7] = [1/32, 0, -59/96, 0, 2/3, -1/12, 0]
            for i in range(4, N - 4):
                Qx[i, i - 2:i + 3] = [1/12, -2/3, 0, 2/3, -1/12]
            for i in range(4):
                Qx[-1-i, -(4+i):] = -Qx[i, 0:(4+i)][::-1]


            Dx = np.matmul(np.linalg.inv(P), Qx)

        elif case == "finite volume":
            if np.linalg.norm(x_mesh - np.linspace(0, 1, N)) < 1e-10:
                dx_equi = x_mesh[1] - x_mesh[0]
                x_mesh[1:-1] += np.random.normal(0, dx_equi/5, np.alen(x_mesh[1:-1]))
                x_mesh.sort()

            dx = np.zeros(N)
            dx[0] = (x_mesh[1] - x_mesh[0])/2
            dx[-1] = (x_mesh[-1] - x_mesh[-2])/2
            dx[1:-1] = (x_mesh[2:] - x_mesh[:-2])/2

            P = np.zeros([N, N])
            for i in range(N):
                P[i, i] = dx[i]

            Qx = np.zeros([N, N])
            for i in range(1, N - 1):
                Qx[i, i - 1:i + 2] = [-0.5, 0, 0.5]
            Qx[0, 0:2] = [-0.5, 0.5]
            Qx[-1, -2:] = [-0.5, 0.5]
            Dx = np.matmul(np.linalg.inv(P), Qx)

        else:
            raise NameError("Invalid differentiation method chosen\n"
                            "Valid names are: \n"
                            "tridiagonal finite difference \n"
                            "pentadiagonal finite difference \n"
                            "finite volume")

        f = lambda x: np.array([1 - x]).T

        self.u = lambda x, t: 1 - np.e ** ((x - self.a) / self.epsilon)
        ux = lambda x, t: - (1 / self.epsilon) * np.e ** ((x - self.a) / self.epsilon)

        g1 = lambda t: (self.a * self.u(0, t) - self.epsilon * ux(0, t))
        g2 = lambda t: (self.epsilon * ux(1, t))

        self.ts = ts_used

        self.sim = OneDSim(self.a, self.epsilon, x_mesh, P, Dx, f, g1, g2, dt=dt, Ut_norm_final=Ut_norm_final,
                           debug=debug)
        self.P = P

    def solve(self):
        self.sim.solve(self.ts)
