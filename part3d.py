from modules.OneDSim import OneDSimDriver
from modules import time_stepping_schemes as ts
import numpy as np
import matplotlib.pyplot as plt
# import matplotlib
# matplotlib.rc('text', usetex=True)
# matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amsmath}"]

N_init = 40
samples = 5

N = np.array([N_init]*samples)


def generate_random_mesh(N):
    x_mesh = np.linspace(0, 1, N)
    dx_equi = x_mesh[1] - x_mesh[0]
    x_mesh[1:-1] += np.random.normal(0, dx_equi / 5, np.alen(x_mesh[1:-1]))
    x_mesh.sort()
    return x_mesh


def refine_mesh(mesh):
    mids = (mesh[1:] + mesh[:-1])/2
    places = np.arange(1, np.alen(mesh))
    return np.insert(mesh, places, mids)


case = ["tridiagonal finite difference", "finite volume", "pentadiagonal finite difference"]
marker = ["^", "o", "s"]
err = []
u = lambda x: np.array([1 - np.e ** ((x - 1)/0.1)]).T

for i in range(len(case)):
    print("******* solve case: ", case[i], " ********")
    err_case = np.array([])
    N_used = N_init
    for j in range(samples):
        if i ==0:
            if j == 0:
                N[j] = N_init
            else:
                N[j] = 2 * N[j-1] -1
        print("N = ", N[j])
        if case[i] == "tridiagonal finite difference":
            sim_driver = OneDSimDriver(N[j], case[i], dt=9/(N[j]**2), ts_used=ts.FE, Ut_norm_final=1e-13)

        elif case[i] == "pentadiagonal finite difference":
            sim_driver = OneDSimDriver(N[j], case[i], dt=5/(N[j]**2), ts_used=ts.FE, Ut_norm_final=1e-13)

        elif case[i] == "finite volume":
            if j == 0:
                mesh = generate_random_mesh(N[j])
            else:
                mesh = refine_mesh(mesh)

            sim_driver = OneDSimDriver(N[j], case[i], dt=2/(N[j]**2), ts_used=ts.FE, Ut_norm_final=1e-11, x_mesh=mesh)

        else:
            raise NameError("method error")

        u_an = np.array([sim_driver.u(sim_driver.sim.x, 0)]).T
        T_h = sim_driver.sim.calc_Ut(u_an)

        e_p = np.sqrt(np.matmul(T_h.T, np.matmul(sim_driver.P, T_h)))
        err_case = np.append(err_case, e_p[0, 0])

    err.append(err_case)

N_n1 = 1/N
N2_N1 = N[1:] / N[:-1]
print("1/N: ", N_n1)

file_object = open("./results/trunc_convergence.txt", "w")

file_object.write("1/N: ")
np.savetxt(file_object, N_n1)
file_object.write("\n")

for i in range(len(case)):
    e1_e2 = err[i][:-1] / err[i][1:]
    grad = np.log(e1_e2) / np.log(N2_N1)


    file_object.write(case[i] + "\n")
    file_object.write("error: ")
    np.savetxt(file_object, err[i])
    file_object.write("\n")

    file_object.write("grad: ")
    np.savetxt(file_object, grad)
    file_object.write("\n\n")

    print("error: ", err[i])
    print(case[i], "grad = ", grad )
    plt.loglog(N_n1, err[i], label=case[i], color="black", marker=marker[i], markeredgecolor="black", markerfacecolor="None")

file_object.close()

plt.grid()
plt.title("Order of truncation error study")
plt.ylabel("$||T_h||_P$")
plt.xlabel("$1/N$")
plt.legend()
plt.figure(1).set_figwidth(10)
plt.figure(1).set_figheight(5)
plt.figure(1).tight_layout()

plt.figure(1).savefig("./document/figures/part3d.png")
plt.show()
